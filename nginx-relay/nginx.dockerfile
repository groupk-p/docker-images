ARG BASE_IMAGE=nginx:1.17-alpine
FROM $BASE_IMAGE
RUN apk add --no-cache bash gettext
COPY nginx.conf.template /etc/nginx/nginx.conf.template
COPY server.template /etc/nginx/server.template

COPY entrypoint.sh /usr/local/bin/weecop-entrypoint.sh
RUN chmod +x /usr/local/bin/weecop-entrypoint.sh
RUN ln -s /usr/local/bin/weecop-entrypoint.sh / # backwards compat

ENTRYPOINT ["weecop-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]