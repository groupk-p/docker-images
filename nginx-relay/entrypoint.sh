#!/bin/bash

SERVERS="$(for SERVER_CONFIG in $(echo $HOSTS | sed "s/,/ /g")
do
  HOST=$(echo $SERVER_CONFIG | cut -f1 -d\|)
  TARGET=$(echo $SERVER_CONFIG | cut -f2 -d\|)
  export SERVER_HOST=$HOST && export SERVER_TARGET=$TARGET && cat /etc/nginx/server.template | envsubst '$SERVER_HOST:$SERVER_TARGET'
done)"

(export SERVERS=$SERVERS && envsubst '$SERVERS' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf)

exec "$@"

