ARG BASE_IMAGE=php:5.6-fpm-alpine
FROM $BASE_IMAGE

RUN apk add --no-cache mariadb-client curl libxml2-dev libzip-dev

RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev && \
  docker-php-ext-configure gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ && \
  docker-php-ext-install gd && \
  apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

RUN apk add --update --no-cache \
    libgcc libstdc++ libx11 glib libxrender libxext libintl icu-dev \
    ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family

RUN docker-php-ext-install pdo pdo_mysql opcache mysqli pcntl bcmath intl mbstring xml zip calendar

RUN set -ex \
    && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool \
    && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
    && pecl install imagick-3.4.3 \
    && docker-php-ext-enable imagick \
    && apk add --no-cache --virtual .imagick-runtime-deps imagemagick \
    && apk del .phpize-deps

# Add ssmtp to relay client emails to our sending server or custom one
RUN apk add --no-cache msmtp
ENV EMAILS_TLS_CERTCHECK=off
ENV EMAILS_STARTTLS=off
ENV EMAILS_TLS=on

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php.ini
COPY php.ini /usr/local/etc/php/conf.d/zz-weecop.ini
RUN (rm /usr/local/etc/php-fpm.d/www.conf || true) && (rm /usr/local/etc/php-fpm.d/www.conf.default || true)
COPY php-fpm-pool.conf /usr/local/etc/php-fpm.d/pool.conf

COPY entrypoint.sh /usr/local/bin/weecop-entrypoint.sh
RUN chmod +x /usr/local/bin/weecop-entrypoint.sh
RUN ln -s /usr/local/bin/weecop-entrypoint.sh / # backwards compat

ENTRYPOINT ["weecop-entrypoint.sh"]
CMD ["php-fpm"]