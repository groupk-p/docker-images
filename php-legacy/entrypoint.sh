#!/bin/sh

echo "defaults" > /tmp/.msmtprc
echo "logfile /dev/null" >> /tmp/.msmtprc
echo "syslog on" >> /tmp/.msmtprc
echo "dsn_notify off" >> /tmp/.msmtprc
echo "dsn_return off" >> /tmp/.msmtprc
echo "account all" >> /tmp/.msmtprc
echo "host $EMAILS_RELAY_HOST" >> /tmp/.msmtprc
if [ -n "$EMAILS_RELAY_PORT" ]; then
  echo "port $EMAILS_RELAY_PORT" >> /tmp/.msmtprc
fi
echo "protocol smtp" >> /tmp/.msmtprc

if [ -n "$EMAILS_RELAY_USER" ]; then
  echo "auth on" >> /tmp/.msmtprc
  echo "user $EMAILS_RELAY_USER" >> /tmp/.msmtprc
  echo "password $EMAILS_RELAY_PASSWORD" >> /tmp/.msmtprc
fi

echo "tls $EMAILS_TLS" >> /tmp/.msmtprc
echo "tls_starttls $EMAILS_STARTTLS" >> /tmp/.msmtprc
echo "tls_certcheck $EMAILS_TLS_CERTCHECK" >> /tmp/.msmtprc
echo "from $EMAILS_DEFAULT_FROM" >> /tmp/.msmtprc
echo "account default : all" >> /tmp/.msmtprc

chmod 400 /tmp/.msmtprc

exec docker-php-entrypoint "$@"