ARG BASE_IMAGE=mariadb:10.5
FROM $BASE_IMAGE

COPY conf.d/weecop_optimize_wordpress.cnf /etc/mysql/conf.d/
COPY conf.d/weecop_optimize_wordpress_shared.cnf /etc/mysql/conf.d/
RUN chmod -R 444 /etc/mysql/conf.d