FROM nginx:1.19.2

RUN chown -R 1000:1000 /var/cache/nginx && chmod -R g+w /var/cache/nginx \
	&& touch /var/run/nginx.pid && chown -R 1000:1000 /var/run/nginx.pid

ADD html /var/www/html/
COPY ./nginx.conf /etc/nginx/nginx.conf