ARG BASE_IMAGE=nginx:1.17-alpine
FROM $BASE_IMAGE
COPY nginx.conf /etc/nginx/nginx.conf