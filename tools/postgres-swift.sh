#!/bin/bash

set -x
set -eo pipefail

# add credentials
echo "*:*:*:${POSTGRES_TARGET_DATABASE_USER}:${POSTGRES_TARGET_DATABASE_PASSWORD}" > /root/.pgpass && chmod 600 /root/.pgpass

# Loop through all the defined databases, seperating by a ,
DUMP=`date +$BACKUP_TIMESTAMP`.sql.gz

echo "exporting to ${SWIFT_OBJECT_NAME}${DUMP} in container ${SWIFT_CONTAINER}"


pg_dumpall -c -w \
--username=${POSTGRES_TARGET_DATABASE_USER} \
--port=${POSTGRES_TARGET_DATABASE_PORT} \
--host=${POSTGRES_TARGET_DATABASE_HOST} | \
gzip -c ${GZIP_ARGUMENTS} | \
swift upload -H "X-Delete-After: $SWIFT_DELETE_AFTER" --segment-size 104857600 --object-name ${SWIFT_OBJECT_NAME}${DUMP} ${SWIFT_CONTAINER} -


