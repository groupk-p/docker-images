#!/bin/bash

set -x
set -eo pipefail

# Set the has_failed variable to false. This will change if any of the subsequent database backups/uploads fail.
has_failed=false

# Loop through all the defined databases, seperating by a ,
DUMP=$CURRENT_DATABASE`date +$BACKUP_TIMESTAMP`.sql.gz

MYSQL_TARGET_DATABASE_NAMES="${MYSQL_TARGET_DATABASE_NAMES} ${MYSQL_TARGET_DATABASE_NAME}"

echo "exporting to ${SWIFT_OBJECT_NAME}${DUMP} in container ${SWIFT_CONTAINER}"

mysqldump --add-drop-table \
--user=${MYSQL_TARGET_DATABASE_USER} \
--port=${MYSQL_TARGET_DATABASE_PORT} \
--host=${MYSQL_TARGET_DATABASE_HOST} \
--password=${MYSQL_TARGET_DATABASE_PASSWORD} \
--databases ${MYSQL_TARGET_DATABASE_NAMES} | \
gzip -c ${GZIP_ARGUMENTS} | \
swift upload -H "X-Delete-After: $SWIFT_DELETE_AFTER" --segment-size 104857600 --object-name ${SWIFT_OBJECT_NAME}${DUMP} ${SWIFT_CONTAINER} -
