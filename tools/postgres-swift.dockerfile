FROM python:3.8-alpine as base

FROM base as builder
RUN mkdir /install
WORKDIR /install
RUN apk add gcc
RUN apk add musl-dev linux-headers
RUN pip install --prefix=/install -Iv python-swiftclient==3.9.0
RUN pip install --prefix=/install -Iv python-keystoneclient==4.0.0
RUN rm -R /install/share/man
RUN rm -R /install/bin/oslo-config-generator
RUN rm -R /install/bin/oslo-config-validator
RUN rm -R /install/bin/pbr

FROM base
COPY --from=builder /install /usr/local
WORKDIR /app

RUN apk add --no-cache bash gzip postgres
COPY postgres-swift.sh /postgres-swift.sh
RUN chmod +x /postgres-swift.sh

ENV POSTGRES_TARGET_DATABASE_HOST ""
ENV POSTGRES_TARGET_DATABASE_PORT "5432"
ENV POSTGRES_TARGET_DATABASE_USER "postgres"
ENV POSTGRES_TARGET_DATABASE_PASSWORD ""
ENV BACKUP_TIMESTAMP "%Y_%m_%d_%H_%M"
ENV OS_USER_DOMAIN_NAME "Default"
ENV OS_PROJECT_DOMAIN_NAME "Default"

CMD ["/postgres-swift.sh"]