#!/bin/bash

set -x
set -eo pipefail

# Loop through all the defined databases, seperating by a ,
DUMP=$OUTPUT_PREFIX`date +$BACKUP_TIMESTAMP`.zip

echo "exporting to ${SWIFT_OBJECT_NAME}${DUMP} in container ${SWIFT_CONTAINER}"


cd ${DIRECTORY_TO_BACKUP} && \
zip -0 -r - * | \
swift upload -H "X-Delete-After: $SWIFT_DELETE_AFTER" --segment-size 104857600 --object-name ${SWIFT_OBJECT_NAME}${DUMP} ${SWIFT_CONTAINER} -
