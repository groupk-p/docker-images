FROM python:3.8-alpine as base

FROM base as builder
RUN mkdir /install
WORKDIR /install
RUN apk add gcc
RUN apk add musl-dev linux-headers
RUN pip install --prefix=/install -Iv python-swiftclient==3.9.0
RUN pip install --prefix=/install -Iv python-keystoneclient==4.0.0
RUN rm -R /install/share/man
RUN rm -R /install/bin/oslo-config-generator
RUN rm -R /install/bin/oslo-config-validator
RUN rm -R /install/bin/pbr

FROM base
COPY --from=builder /install /usr/local
WORKDIR /app

RUN apk add --no-cache bash zip
COPY zip-swift.sh /zip-swift.sh
RUN chmod +x /zip-swift.sh

ENV DIRECTORY_TO_BACKUP "/var/www/html"
ENV OUTPUT_PREFIX "files-"
ENV BACKUP_TIMESTAMP "%Y_%m_%d_%H_%M"
ENV OS_USER_DOMAIN_NAME "Default"
ENV OS_PROJECT_DOMAIN_NAME "Default"

CMD ["/zip-swift.sh"]