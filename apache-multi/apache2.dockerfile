FROM httpd:2.4-alpine
COPY httpd2.conf /usr/local/apache2/conf/httpd.conf
COPY httpd2-mpm.conf /usr/local/apache2/conf/extra/httpd-mpm.conf